---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: minio
  labels:
    app.kubernetes.io/name: minio
  annotations:
    reloader.stakater.com/auto: 'true'
spec:
  replicas: 3
  selector:
    matchLabels:
      app.kubernetes.io/name: minio
  serviceName: minio-headless
  template:
    metadata:
      labels:
        app.kubernetes.io/name: minio
    spec:
      containers:
        - name: minio
          image: minio/minio:RELEASE.2023-07-07T07-13-57Z@sha256:760ce64b2ae944a1d9daa713c409a320eb4524e0fa9bb73852b9701ee00cb15c
          imagePullPolicy: IfNotPresent
          args:
            - server
          env:
            - name: MINIO_ADDRESS
              value: "0.0.0.0:9000"
            - name: MINIO_CONSOLE_ADDRESS
              value: "0.0.0.0:8080"
            - name: MINIO_VOLUMES
              value: "http://minio-{0...2}.minio-headless.minio.svc.cluster.local/data"
            - name: MINIO_ROOT_USER
              valueFrom:
                secretKeyRef:
                  name: minio-admin
                  key: username
                  optional: false
            - name: MINIO_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: minio-admin
                  key: password
                  optional: false
            - name: MINIO_IDENTITY_OPENID_VENDOR
              value: keycloak
            - name: MINIO_IDENTITY_OPENID_DISPLAY_NAME
              value: "Login with Keycloak"
            - name: MINIO_SERVER_URL
              valueFrom:
                secretKeyRef:
                  name: minio
                  key: server-url
                  optional: false
            - name: MINIO_BROWSER_REDIRECT_URL
              valueFrom:
                secretKeyRef:
                  name: minio
                  key: browser-url
                  optional: false
            - name: MINIO_IDENTITY_OPENID_CONFIG_URL
              valueFrom:
                secretKeyRef:
                  name: minio-keycloak
                  key: metadata-url
                  optional: false
            - name: MINIO_IDENTITY_OPENID_KEYCLOAK_ADMIN_URL
              valueFrom:
                secretKeyRef:
                  name: minio-keycloak
                  key: admin-url
                  optional: false
            - name: MINIO_IDENTITY_OPENID_CLIENT_ID
              valueFrom:
                secretKeyRef:
                  name: minio-keycloak
                  key: client-id
                  optional: false
            - name: MINIO_IDENTITY_OPENID_CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                  name: minio-keycloak
                  key: client-secret
                  optional: false
            - name: MINIO_IDENTITY_OPENID_SCOPES
              value: "email,openid"
            - name: MINIO_IDENTITY_OPENID_CLAIM_NAME
              value: "roles"
          ports:
            - name: minio
              containerPort: 9000
            - name: http
              containerPort: 8080
          readinessProbe:
            httpGet:
              path: /minio/health/ready
              port: 9000
            initialDelaySeconds: 120
            periodSeconds: 20
          livenessProbe:
            httpGet:
              path: /minio/health/live
              port: 9000
            initialDelaySeconds: 120
            periodSeconds: 20
          securityContext:
            runAsUser: 1000
            runAsNonRoot: true
            allowPrivilegeEscalation: false
            capabilities:
              drop:
                - ALL
            privileged: false
            readOnlyRootFilesystem: false
            seccompProfile:
              type: RuntimeDefault
          volumeMounts:
            - name: data
              mountPath: /data
      securityContext:
        fsGroup: 1000
        runAsNonRoot: true
        fsGroupChangePolicy: "OnRootMismatch"
      serviceAccount: minio
      serviceAccountName: minio
  volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "local-storage"
        resources:
          requests:
            storage: 1
